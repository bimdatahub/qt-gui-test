#-------------------------------------------------
#
# Project created by QtCreator 2014-10-13T13:08:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bim
TEMPLATE = app


SOURCES += main.cpp\
        bim.cpp

HEADERS  += bim.h
