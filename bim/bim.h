#ifndef BIM_H
#define BIM_H

#include <QDialog>

QT_BEGIN_NAMESPACE
class QCheckBox;
class QLabel;
class QErrorMessage;
QT_END_NAMESPACE

class DialogOptionsWidget;

class bim : public QDialog
{
    Q_OBJECT

public:
    bim(QDialog *parent = 0);
    ~bim();

private slots:
    void setBimAdd();
    void setBimPort();
    void setUsrname();
    void setUsrPass();
    //void setProject();
    //void setIFCFile();
    //void newProject();
    //void newIFCFile();

private:
    QLabel *bimAddLabel;
    QLabel *bimPortLabel;
    QLabel *usrNameLabel;
    QLabel *usrPassLabel;
    //QLabel *project;
    //QLabel *IFCfile;
};

#endif // BIM_H
