#include "bim.h"

#include <QInputDialog>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QToolBox>
#include <QLabel>
#include <QPushButton>
#include <QDialog>

class DialogOptionsWidget : public QGroupBox
{
public:
    explicit DialogOptionsWidget(QWidget *parent = 0);

    void addCheckBox(const QString &text, int value);
    void addSpacer();
    int value() const;

private:
    typedef QPair<QCheckBox *, int> CheckBoxEntry;
    QVBoxLayout *layout;
    QList<CheckBoxEntry> checkBoxEntries;
};

DialogOptionsWidget::DialogOptionsWidget(QWidget *parent) :
    QGroupBox(parent) , layout(new QVBoxLayout)
{
    setTitle(bim::tr("Options"));
    setLayout(layout);
}

void DialogOptionsWidget::addCheckBox(const QString &text, int value)
{
    QCheckBox *checkBox = new QCheckBox(text);
    layout->addWidget(checkBox);
    checkBoxEntries.append(CheckBoxEntry(checkBox, value));
}

void DialogOptionsWidget::addSpacer()
{
    layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Ignored, QSizePolicy::MinimumExpanding));
}

int DialogOptionsWidget::value() const
{
    int result = 0;
    foreach (const CheckBoxEntry &checkboxEntry, checkBoxEntries)
        if (checkboxEntry.first->isChecked())
            result |= checkboxEntry.second;
    return result;
}

bim::bim(QDialog *parent)
    : QDialog(parent)
{
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    QToolBox *toolbox = new QToolBox;
    mainLayout->addWidget(toolbox);

    int frameStyle = QFrame::Sunken | QFrame::Panel;

    bimAddLabel = new QLabel;
    bimAddLabel->setFrameStyle(frameStyle);
    QPushButton *bimAddButton =
            new QPushButton(tr("Insert Bimserver Address"));

    bimPortLabel = new QLabel;
    bimPortLabel->setFrameStyle(frameStyle);
    QPushButton *bimPortButton =
            new QPushButton(tr("Insert Bimserver Port Number"));

    usrNameLabel = new QLabel;
    usrNameLabel->setFrameStyle(frameStyle);
    QPushButton *usrNameButton =
            new QPushButton(tr("Insert Username for Server"));

    usrPassLabel = new QLabel;
    usrPassLabel->setFrameStyle(frameStyle);
    QPushButton *usrPassButton =
            new QPushButton(tr("Insert Passcode for Server"));
    /*
    project = new QLabel;
    project->setFrameStyle(frameStyle);
    QPushButton *projectButton =
            new QPushButton(tr("Choose the appropriate project"));

    IFCfile = new QLabel;
    IFCfile->setFrameStyle(frameStyle);
    QPushButton *IFCfileButton =
            new QPushButton(tr("Choose the appropriate IFC file"));
    */

    QPushButton *acceptButton =
            new QPushButton(tr("Ok"));
    QPushButton *rejectButton =
            new QPushButton(tr("Cancel"));

    connect(bimAddButton, SIGNAL(clicked()), this, SLOT(setBimAdd()));
    connect(bimPortButton, SIGNAL(clicked()), this, SLOT(setBimPort()));
    connect(usrNameButton, SIGNAL(clicked()), this, SLOT(setUsrname()));
    connect(usrPassButton, SIGNAL(clicked()), this, SLOT(setUsrPass()));

    connect(acceptButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(rejectButton, SIGNAL(clicked()), this, SLOT(reject()));

    QWidget *page = new QWidget;
    QGridLayout *layout = new QGridLayout(page);
    layout->setColumnStretch(1, 1);
    layout->setColumnMinimumWidth(1, 250);
    layout->addWidget(bimAddButton, 0, 0);
    layout->addWidget(bimAddLabel, 0, 1);
    layout->addWidget(bimPortButton, 1, 0);
    layout->addWidget(bimPortLabel, 1, 1);
    layout->addWidget(usrNameButton, 2, 0);
    layout->addWidget(usrNameLabel, 2, 1);
    layout->addWidget(usrPassButton, 3, 0);
    layout->addWidget(usrPassLabel, 3, 1);
    layout->addWidget(acceptButton, 4, 0);
    layout->addWidget(rejectButton, 4, 1);
    layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Ignored, QSizePolicy::MinimumExpanding), 5, 0);
    toolbox->addItem(page, tr("Bimserver Log-on"));
    setWindowTitle(tr("IFC import"));

    //emit signal after user press "ok"
}

bim::~bim()
{

}

void bim::setBimAdd()
{
//! [0]
    bool ok;
    QString text = QInputDialog::getText(this, tr("Add the Bimserver IP address"),
                                 tr("hostname/ip address:"), QLineEdit::Normal,
                                 "localhost", &ok);
    if (ok && !text.isEmpty())
        bimAddLabel->setText(text);
//! [0]
}

void bim::setBimPort()
{
//! [1]
    bool ok;
    int i = QInputDialog::getInt(this, tr("Add the Bimserver Port Number"),
                                 tr("hostname/ip address:"),
                                 14555, 1, 65535, 1, &ok);
    if (ok)
        bimPortLabel->setText(tr("%1").arg(i));
//! [1]
}

void bim::setUsrname()
{
//! [2]
    bool ok;
    QString text = QInputDialog::getText(this, tr("Add the username"),
                                 tr("username:"), QLineEdit::Normal,
                                 "admin", &ok);
    if (ok && !text.isEmpty())
        usrNameLabel->setText(text);
//! [2]
}

void bim::setUsrPass()
{
//! [0]
    bool ok;
    QString text = QInputDialog::getText(this, tr("Add the user passcode"),
                                 tr("passcode:"), QLineEdit::Normal,
                                 "123456", &ok);
    if (ok && !text.isEmpty())
        usrPassLabel->setText(text);
//! [0]
}
